export namespace TunnelConfig {
  export interface Tunnel {
    key: string;
    target: {
      port: number;
      url: string;
    };
    virtualHost: string;
  }
  export type Tunnels = ReadonlyArray<Tunnel>;
  export interface Serializer {
    deserialize: (configStr: string) => Tunnels;
    serialize: (tunnels: Tunnels) => string;
  }
}
