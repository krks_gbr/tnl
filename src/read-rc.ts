import * as fs from "fs";
import * as os from "os";
import * as path from "path";
import { SSH } from "./remote/ssh";

const HOME = os.homedir();
const DEFAULT_PRIVATE_KEY_PATH = path.resolve(HOME, ".ssh", "id_rsa");
const LOCAL_RC = ".tnlrc";

function validateRC(data: any) {
  const requiredKeys = ["host", "username"];
  const actualKeys = Object.keys(data);
  const match = actualKeys.filter(key => requiredKeys.includes(key));
  if (match.length !== requiredKeys.length) {
    throw new Error(
      `Expected to find keys: [${requiredKeys.join(
        ", "
      )}], instead found [${actualKeys.join(", ")}]`
    );
  }
  return data;
}

function readPrivateKey(filepath: string) {
  const readpath = filepath || DEFAULT_PRIVATE_KEY_PATH;
  try {
    return fs.readFileSync(readpath, "utf8");
  } catch (e) {
    throw new Error(
      `Error reading private key at path: "${readpath}". Details: ${e.message}`
    );
  }
}

export function readRC(): SSH.ConnectConfig {
  try {
    const rcFile = fs.readFileSync(path.resolve(HOME, LOCAL_RC), "utf8");
    const rc = JSON.parse(rcFile);
    const validRC = validateRC(rc);
    return {
      host: validRC.host,
      port: validRC.port,
      username: validRC.username,
      privateKey: readPrivateKey(validRC.privateKey)
    };
  } catch (e) {
    throw new Error(`Failed to read configuration file. Details: ${e.message}`);
  }
}
