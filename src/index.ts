import * as ora from "ora";
import chalk from "chalk";
import { CLI } from "./cli/index";
import { Remote } from "./remote/index";
import { SSH } from "./remote/ssh";
import { API } from "./api/index";
import { Traefik } from "./traefik/index";
import { readRC } from "./read-rc";

(async function main() {
  const spinner = ora("Connecting...").start();
  try {
    const connectConfig = readRC();
    const conn = await SSH.connect(connectConfig);
    const serverConfig = await Remote.readServerConfig(conn);
    spinner.stop();
    const remote = Remote.init(conn, serverConfig, Traefik.serializer);
    const api = API.init(remote);
    const cli = CLI.init(api);

    if (!process.argv.slice(2).length) {
      cli.outputHelp();
      process.exit(0);
    }

    cli.parse(process.argv);
  } catch (e) {
    spinner.fail(chalk.red(e.message));
    process.exit(1);
  }
})();

process.on("unhandledRejection", err => {
  console.log(chalk.red(`\nUnhandled rejection:\n${err.stack}`));
  process.exit(1);
});
