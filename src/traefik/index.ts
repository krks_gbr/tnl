import * as toml from "toml-j0.4";
import * as tomlify from "tomlify-j0.4";
import * as Debug from "debug";
import * as URL from "url";
import { TunnelConfig } from "../tunnel-config/index";

const debug = Debug("tnl:tunnelConfig");

export namespace Traefik {
  export interface Config {
    backends: {
      [key: string]: {
        servers: {
          server: {
            url: string;
          };
        };
      };
    };
    frontends: {
      [key: string]: {
        backend: string;
        routes: {
          route: {
            rule: string;
          };
        };
      };
    };
  }

  export function listFromConfiguration(
    configuration: Config
  ): TunnelConfig.Tunnels {
    if (Object.keys(configuration).length === 0) {
      return [];
    }
    const tunnels = Object.keys(configuration.backends).map(key => {
      const url = configuration.backends[key].servers.server.url;
      const parsedURL = URL.parse(url);
      const hostRule = configuration.frontends[key].routes.route.rule;
      const host = hostRule.split("Host:")[1];
      return {
        key,
        virtualHost: host,
        target: {
          url: parsedURL.hostname,
          port: parseInt(parsedURL.port)
        }
      };
    });
    debug("tunnels:\n:", JSON.stringify(tunnels, null, 4));
    return tunnels;
  }

  export function listToConfiguration(tunnels: TunnelConfig.Tunnels): Config {
    return tunnels.reduce(
      (a, t) => {
        return {
          ...a,
          backends: {
            ...a.backends,
            [t.key]: {
              servers: {
                server: {
                  url: `"http://${t.target.url}:${t.target.port}"`
                }
              }
            }
          },
          frontends: {
            ...a.frontends,
            [t.key]: {
              backend: `"${t.key}"`,
              routes: {
                route: {
                  rule: `"Host:${t.virtualHost}"`
                }
              }
            }
          }
        };
      },
      { backends: {}, frontends: {} }
    );
  }

  export const serializer: TunnelConfig.Serializer = {
    deserialize(configStr: string): TunnelConfig.Tunnels {
      debug(`deserializing config:\n${configStr}`);
      const parsedConfig = toml.parse(configStr);
      debug("parsed config:", JSON.stringify(parsedConfig, null, 4));
      try {
        return listFromConfiguration(parsedConfig);
      } catch (e) {
        throw new Error(
          "Your tunnel configuration file does not seem to be correct."
        );
      }
    },
    serialize(tunnels: TunnelConfig.Tunnels): string {
      const config = listToConfiguration(tunnels);
      return tomlify
        .toToml(config, { space: 2 })
        .split("\n")
        .filter((l: string) => l !== "")
        .join("\n");
    }
  };
}
