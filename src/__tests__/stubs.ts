import { SSH } from "../remote/ssh";

export const stubConnection = (jest: any): SSH.Connection => {
  return {
    configuration: jest.fn(() => {
      return {
        host: "",
        port: 22,
        username: "",
        privateKey: ""
      };
    }),
    on: jest.fn(() => {}),
    exec: jest.fn(() => {
      return Promise.resolve(`exec result`);
    }),
    forwardIn: jest.fn(() => {
      return Promise.resolve({});
    })
  };
};
