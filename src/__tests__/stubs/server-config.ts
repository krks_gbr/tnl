import { Remote } from "../../remote/index";
import { mockTunnelPortRange } from "./tunnel-config";

export const mockServerConfig: Remote.ServerConfig = {
  portRange: mockTunnelPortRange,
  tunnelConfig: "/some/path",
  restartCmd: "restart cmd"
};
