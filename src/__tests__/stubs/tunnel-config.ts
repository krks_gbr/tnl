import { TunnelConfig } from "../../tunnel-config/index";
import { Remote } from "../../remote/index";

export const invalidToml = "this is an invalid toml config string";

export const validTomlInvalidConfig = `
[[fruit]]
  name = "apple"

  [fruit.physical]
    color = "red"
    shape = "round"
`;

export const validToml = `
[backends]
  [backends.tunnel1]
    [backends.tunnel1.servers]
      [backends.tunnel1.servers.server]
      url = "http://test.com:45555"
  [backends.tunnel2]
    [backends.tunnel2.servers]
      [backends.tunnel2.servers.server]
      url = "http://test.com:45556"
[frontends]
  [frontends.tunnel1]
  backend = "tunnel1"
    [frontends.tunnel1.routes]
      [frontends.tunnel1.routes.route]
      rule = "Host:tunnel1.test.com"
  [frontends.tunnel2]
  backend = "tunnel2"
    [frontends.tunnel2.routes]
      [frontends.tunnel2.routes.route]
      rule = "Host:tunnel2.test.com"
`;

export const tomlJS = {
  backends: {
    tunnel1: {
      servers: {
        server: {
          url: '"http://test.com:45555"'
        }
      }
    },
    tunnel2: {
      servers: {
        server: {
          url: '"http://test.com:45556"'
        }
      }
    }
  },
  frontends: {
    tunnel1: {
      backend: '"tunnel1"',
      routes: {
        route: {
          rule: '"Host:tunnel1.test.com"'
        }
      }
    },
    tunnel2: {
      backend: '"tunnel2"',
      routes: {
        route: {
          rule: '"Host:tunnel2.test.com"'
        }
      }
    }
  }
};

export const mockTunnelPortRange: Remote.TunnelPortRange = [45555, 50000];
export const mockTunnel1 = {
  target: {
    url: "test.com",
    port: 45555
  },
  key: "tunnel1",
  virtualHost: "tunnel1.test.com"
};
export const mockTunnel2 = {
  target: {
    url: "test.com",
    port: 45556
  },
  key: "tunnel2",
  virtualHost: "tunnel2.test.com"
};
export const mockTunnelList: TunnelConfig.Tunnels = [mockTunnel1, mockTunnel2];
