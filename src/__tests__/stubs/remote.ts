import { mockTunnelList, mockTunnelPortRange } from "./tunnel-config";
import { Remote } from "../../remote/index";
import { TunnelConfig } from "../../tunnel-config/index";

export const netstatResult = `
localhost:domain
*:ssh
localhost:953
*:52698
*:20000
*:45555
`;

export const connectedPortsResult = [953, 52698, 20000, 45555];

export function stubRemote({
  readResult = mockTunnelList,
  writeResult = "written",
  portRange = mockTunnelPortRange
}: {
  readResult?: TunnelConfig.Tunnels | "error";
  writeResult?: string | "error";
  portRange?: Remote.TunnelPortRange;
}): Remote.Interface {
  return {
    host: jest.fn(() => {
      return "test.host.com";
    }),
    portRange: jest.fn(() => {
      return portRange;
    }),
    readTunnelConfig: jest.fn(() => {
      if (readResult === "error") {
        return Promise.reject(new Error("error reading"));
      }
      return Promise.resolve(readResult);
    }),
    connectedPorts: jest.fn(() => {
      return Promise.resolve(connectedPortsResult);
    }),
    writeTunnelConfig: jest.fn(() => {
      if (writeResult === "error") {
        return Promise.reject(new Error("error writing"));
      }
      return Promise.resolve(writeResult);
    }),
    tunnel: jest.fn(() => {
      return Promise.resolve({});
    }),
    restart: jest.fn(() => {
      return Promise.resolve({});
    })
  };
}
