import { SSH } from "../../remote/ssh";

interface Opts {
  execResult?: string;
}

const defaultOpts = {
  execResult: "stub exec result"
};

export const stubSSHConnection = (
  jest: any,
  opts: Opts = defaultOpts
): SSH.Connection => {
  return {
    configuration: jest.fn(() => {
      return {
        host: "",
        port: 22,
        username: "",
        privateKey: ""
      };
    }),
    exec: jest.fn(() => {
      return Promise.resolve(opts.execResult);
    }),
    on: jest.fn(() => {}),
    forwardIn: jest.fn(() => {
      return Promise.resolve("");
    })
  };
};
