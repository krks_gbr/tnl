import { TunnelConfig } from "../../tunnel-config/index";

export const serializedResult = "";
export const deserializedResult: TunnelConfig.Tunnels = [];

export const stubSerializer = (): TunnelConfig.Serializer => {
  return {
    serialize: jest.fn(() => serializedResult),
    deserialize: jest.fn(() => deserializedResult)
  };
};
