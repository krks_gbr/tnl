import {
  mockTunnel1,
  mockTunnelList,
  mockTunnelPortRange
} from "./stubs/tunnel-config";
import { stubRemote } from "./stubs/remote";
import { API } from "../api/index";

// import * as Debug from "debug";
// const debug = Debug("test:api");

function createReadResultWithPorts(ports: number[]) {
  return ports.map(port => {
    return {
      ...mockTunnel1,
      target: {
        ...mockTunnel1.target,
        port
      },
      key: `${mockTunnel1.key}-${port}`
    };
  });
}

function last(list: any[]) {
  return list[list.length - 1];
}

describe("API.init()", () => {
  test("works", () => {
    const remote = stubRemote({});
    expect(() => API.init(remote)).not.toThrow();
  });
});

describe("api.list()", () => {
  test("calls remote.readTunnelConfig", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    await api.list();
    expect(remote.readTunnelConfig).toBeCalled();
  });
});

describe("api.add()", async () => {
  test("calls remote.writeTunnelConfig and remote.restartTraefik", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    await api.add("mocktunnel");
    expect(remote.writeTunnelConfig).toBeCalled();
    expect(remote.restart).toBeCalled();
  });

  test("increases length of tunnels list", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    const oldTunnels = await api.list();
    const newTunnels = await api.add("mocktunnel");
    expect(newTunnels.length).toBe(oldTunnels.length + 1);
  });

  describe("port assignment", () => {
    test("rejects if there are no more available ports left in port range", async () => {
      expect.assertions(1);
      const portRange: [number, number] = [
        mockTunnelPortRange[0],
        mockTunnelPortRange[0] + 1
      ];
      const readResult = createReadResultWithPorts(portRange);
      const remote = stubRemote({ readResult, portRange });
      const api = API.init(remote);
      try {
        await api.add("mocktunnel");
      } catch (e) {
        expect(e.message).toContain("Ran out of assignable ports in range");
      }
    });

    test("if there are unassigned ports in port range, newly added port is assigned to one of them", async () => {
      const portRange: [number, number] = [4000, 4003];
      // the missing port is 4002
      const assignedPorts = [4000, 4001, 4003];
      const readResult = createReadResultWithPorts(assignedPorts);
      const remote = stubRemote({ readResult, portRange });
      const api = API.init(remote);
      const newTunnels = await api.add("mocktunnel");
      expect(last(newTunnels).target.port).toBe(4002);
    });

    test("by default, new tunnel has a port number one larger than tunnel with largest port", async () => {
      const remote = stubRemote({});
      const api = API.init(remote);
      const prevTunnels = await api.list();
      const prevLargestPort = last(
        prevTunnels.map(tunnel => tunnel.target.port)
      );
      const newTunnels = await api.add("mocktunnel");
      expect(last(newTunnels).target.port).toBe(prevLargestPort + 1);
    });
  });

  test("rejects if key is already taken", async () => {
    expect.assertions(1);
    const remote = stubRemote({});
    const api = API.init(remote);
    try {
      await api.add(mockTunnel1.key);
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
  });

  describe("tunnel properties are derived from remote properties", () => {
    const remote = stubRemote({});
    const tunnelKey = "mocktunnel";
    const api = API.init(remote);
    const result = api.add(tunnelKey);
    test("tunnel's target url equals remote's host", async () => {
      expect(last(await result).target.url).toBe(remote.host());
    });
    test("tunnel's virtualhost is of the pattern: <tunnel key>.<remote's host>", async () => {
      expect(last(await result).virtualHost).toBe(
        `${tunnelKey}.${remote.host()}`
      );
    });
  });
});

describe("api.remove()", () => {
  const removeKey = mockTunnelList[0].key;

  test("calls remote.writeTunnelConfig and remote.restartTraefik", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    await api.remove(removeKey);
    expect(remote.writeTunnelConfig).toBeCalled();
    expect(remote.restart).toBeCalled();
  });

  test("rejects if key doesn't exist", async () => {
    const remote = stubRemote({ writeResult: "error" });
    const api = API.init(remote);
    await expect(api.remove("sometunnelthatdoenstexist")).rejects.toBeTruthy();
  });

  test("removing a tunnel decreases length of tunnels list", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    const oldTunnels = await api.list();
    const newTunnels = await api.remove(removeKey);
    expect(newTunnels.length).toBe(oldTunnels.length - 1);
  });
});

describe("api.status()", () => {
  test("works", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    const result = await api.status();
    expect(result).toHaveLength(mockTunnelList.length);
    expect(result[0]).toEqual({
      key: mockTunnelList[0].key,
      status: "connected"
    });
    expect(result[1]).toEqual({
      key: mockTunnelList[1].key,
      status: "disconnected"
    });
  });
});

describe("api.tunnel()", () => {
  test("rejects if key doesn't exist", async () => {
    expect.assertions(1);
    const remote = stubRemote({});
    const api = API.init(remote);
    try {
      await api.tunnel("somenonexistenttunnel", 8000);
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
  });

  test("calls remote.tunnel with the right arguments", async () => {
    const remote = stubRemote({});
    const api = API.init(remote);
    await api.tunnel(mockTunnel1.key, 8000);
    expect(remote.tunnel).toBeCalledWith(
      mockTunnel1.target.port,
      8000,
      undefined
    );
  });
});
