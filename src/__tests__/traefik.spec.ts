import * as toml from "toml-j0.4";
import { Traefik } from "../traefik/index";
import {
  validToml,
  validTomlInvalidConfig,
  mockTunnelList,
  tomlJS
} from "./stubs/tunnel-config";

// import * as Debug from "debug";
// const debug = Debug("test:configurator");

describe("Traefik.listFromConfiguration()", () => {
  test("given an empty configuration, it returns an empty array", async () => {
    const parsedConfig = toml.parse("");
    const result = Traefik.listFromConfiguration(parsedConfig);
    expect(result).toEqual([]);
  });

  test("given a valid configuration, it returns correct result", () => {
    const parsedConfig = toml.parse(validToml);
    const result = Traefik.listFromConfiguration(parsedConfig);
    expect(result).toEqual(mockTunnelList);
  });
});

describe("Traefik.listToConfiguration()", () => {
  test("given a tunnel list it returns correct result", () => {
    const config = Traefik.listToConfiguration(mockTunnelList);
    expect(config).toEqual(tomlJS);
  });
});

describe("Traefik.serializer", () => {
  describe("serialize", () => {
    test("given a tunnel list, it returns a valid toml string", () => {
      const result = Traefik.serializer.serialize(mockTunnelList);
      expect(result.trim().replace(/\\"/g, "")).toEqual(validToml.trim());
    });
  });

  describe("deserialize", () => {
    test("given a valid toml string it returns a tunnel list", () => {
      const config = Traefik.serializer.deserialize(validToml);
      expect(config).toEqual(mockTunnelList);
    });
    test("given a valid toml string which does not contain a proper tunnel config, it throws", () => {
      expect(() =>
        Traefik.serializer.deserialize(validTomlInvalidConfig)
      ).toThrow();
    });
    test("given an empty string it returns and empty array", () => {
      const config = Traefik.serializer.deserialize("");
      expect(config).toEqual([]);
    });
  });
});
