import { stubSerializer, serializedResult } from "./stubs/serializer";
import { stubSSHConnection } from "./stubs/ssh-connection";
import { Remote } from "../remote/index";
import { mockServerConfig } from "./stubs/server-config";
import { netstatResult, connectedPortsResult } from "./stubs/remote";

// import * as Debug from "debug";
// const debug = Debug("tnl:test:remote");

const serializer = stubSerializer();

describe("Remote.readServerConfig()", () => {
  test("fails if JSON is invalid", async () => {
    expect.assertions(1);
    const invalidJSON = "whatever";
    const conn = stubSSHConnection(jest, { execResult: invalidJSON });
    try {
      await Remote.readServerConfig(conn);
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
  });

  test("fails if config does not contain required fields", async () => {
    expect.assertions(1);
    const invalidConfig = { invalidKey: "config" };
    const conn = stubSSHConnection(jest, {
      execResult: JSON.stringify(invalidConfig)
    });
    try {
      await Remote.readServerConfig(conn);
    } catch (e) {
      expect(e.message).toBeTruthy();
    }
  });

  test("works if passed a valid server config", async () => {
    expect.assertions(1);
    const conn = stubSSHConnection(jest, {
      execResult: JSON.stringify(mockServerConfig)
    });
    const result = await Remote.readServerConfig(conn);
    expect(result).toEqual(mockServerConfig);
  });
});

describe("remote.host()", () => {
  test("returns the same host as what ssh connection is configured with", async () => {
    const conn = stubSSHConnection(jest);
    const remote = await Remote.init(conn, mockServerConfig, serializer);
    expect(remote.host()).toBe(conn.configuration().host);
  });
});

describe("remote.tunnelPortRange()", () => {
  test("return the portRange from server config", async () => {
    const conn = stubSSHConnection(jest);
    const remote = await Remote.init(conn, mockServerConfig, serializer);
    expect(remote.portRange()).toBe(mockServerConfig.portRange);
  });
});

describe("remote.readTunnelConfig()", () => {
  test("reads tunnel config file from location defined in server config and returns deserialized result", async () => {
    const execResult = "some result";
    const conn = stubSSHConnection(jest, { execResult });
    const remote = await Remote.init(conn, mockServerConfig, serializer);
    await remote.readTunnelConfig();
    expect(conn.exec).toBeCalledWith(`cat ${mockServerConfig.tunnelConfig}`);
  });
});

describe("remote.writeTunnelConfig()", () => {
  test("serializes tunnel config and writes it to file at location defined in server config", async () => {
    const conn = stubSSHConnection(jest);
    const remote = await Remote.init(conn, mockServerConfig, serializer);
    await remote.writeTunnelConfig([]);
    expect(serializer.serialize).toBeCalledWith([]);
    expect(conn.exec).toBeCalledWith(
      `echo "${serializedResult}" > ${mockServerConfig.tunnelConfig}`
    );
  });
});

describe("remote.connectedPorts()", () => {
  test("work properly", async () => {
    const conn = stubSSHConnection(jest, { execResult: netstatResult });
    const remote = await Remote.init(conn, mockServerConfig, serializer);
    const result = await remote.connectedPorts();
    expect(result).toEqual(connectedPortsResult);
  });
});

describe("remote.restart()", () => {
  test("calls conn.exec() with restartCmd defined in serverConfig", async () => {
    const conn = stubSSHConnection(jest);
    const remote = await Remote.init(conn, mockServerConfig, serializer);
    await remote.restart();
    expect(conn.exec).toBeCalledWith(mockServerConfig.restartCmd);
  });
});
