import { Client as SSH2 } from "ssh2";
import * as Debug from "debug";

const debug = Debug("tnl:ssh");

export namespace SSH {
  export type ConnectConfig = {
    host: string;
    port: number;
    username: string;
    privateKey: string;
  };
  export interface Client {
    connect: (config: ConnectConfig) => Promise<Connection>;
  }

  const on = new SSH2().on;
  export interface Connection {
    configuration: () => ConnectConfig;
    exec: (cmd: string) => Promise<string>;
    forwardIn: (remoteAddr: string, remotePort: number) => Promise<void>;
    on: typeof on;
  }

  export const connect = (config: ConnectConfig) => {
    const client = new SSH2();
    // client.on
    debug(
      "connecting with config",
      JSON.stringify({ ...config, privateKey: "**********" }, null, 4)
    );
    client.connect(config);
    const connection: Connection = {
      configuration: () => config,
      on: client.on.bind(client),
      exec(cmd: string) {
        debug("executing remote command:", cmd);
        return new Promise<string>((resolve, reject) => {
          client.exec(cmd, (err, stream) => {
            if (err) {
              reject(err);
            }

            stream.on("data", (data: string) => {
              debug(`STDOUT: ${data}`);
              resolve(data.toString());
            });

            let errMsg = "";
            stream.stderr.on("data", (data: string) => {
              errMsg += data;
              debug(`STDERR: ${data}`);
            });

            stream.on("close", (code: number, signal: string) => {
              debug(`Stream closed. code: ${code}, signal: ${signal}`);
              if (code !== 0) {
                reject(
                  new Error(
                    `An error occured while executing remote command "${
                      cmd
                    }".\n${errMsg}`
                  )
                );
              }
              resolve("");
            });
          });
        });
      },
      forwardIn(remoteAddr: string, remotePort: number) {
        return new Promise((resolve, reject) => {
          client.forwardIn(remoteAddr, remotePort, err => {
            if (err) {
              reject(err);
            }
            debug(
              `Listening for connections on server on port ${remoteAddr}:${
                remotePort
              }.`
            );
            resolve();
          });
        });
      }
    };
    return new Promise<Connection>((resolve, reject) => {
      client.on("ready", () => {
        debug("connected");
        resolve(connection);
      });

      client.on("error", err => {
        debug("client error", err);
        reject(err);
      });
    });
  };
}
