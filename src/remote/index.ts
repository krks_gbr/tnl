import * as Debug from "debug";
import { SSH } from "./ssh";
import { TunnelConfig } from "../tunnel-config/index";
import { Socket } from "net";

const debug = Debug("tnl:remote");

export namespace Remote {
  export const SERVER_CONFIG_PATH = "~/.tnlrc.server";
  export type TunnelPortRange = Readonly<[number, number]>;
  export interface ServerConfig {
    portRange: TunnelPortRange;
    tunnelConfig: string;
    restartCmd: string;
  }

  export interface TunnelEventListener {
    connection?: (data: any) => any;
    data?: (data: string) => any;
    close?: () => any;
    error?: (err: Error) => any;
  }

  export interface Interface {
    portRange: () => TunnelPortRange;
    host: () => string;
    connectedPorts: () => Promise<number[]>;
    readTunnelConfig: () => Promise<TunnelConfig.Tunnels>;
    writeTunnelConfig: (tunnels: TunnelConfig.Tunnels) => Promise<string>;
    restart: () => Promise<string>;
    tunnel: (
      remotePort: number,
      localPort: number,
      listener: TunnelEventListener
    ) => Promise<void>;
  }

  const validateServerConfig = (serverConfig: any) => {
    const requiredKeys = ["portRange", "tunnelConfig", "restartCmd"];
    const actualKeys = Object.keys(serverConfig);
    const match = requiredKeys.filter(requiredKey =>
      actualKeys.includes(requiredKey)
    );
    return {
      error:
        match.length === requiredKeys.length
          ? null
          : `Expected keys [${requiredKeys.join(
              ", "
            )}] in server config. Instead found [${actualKeys.join(", ")}].`
    };
  };

  export async function readServerConfig(
    conn: SSH.Connection
  ): Promise<ServerConfig> {
    debug("reading server config");
    const serverConfigJSON = await conn.exec(`cat ${SERVER_CONFIG_PATH}`);
    debug("server config json:", serverConfigJSON);
    try {
      const serverConfig = JSON.parse(serverConfigJSON);
      const { error } = validateServerConfig(serverConfig);
      if (error) {
        throw new Error(error);
      }
      return serverConfig;
    } catch (e) {
      throw new Error(`Invalid server configuration. Details: ${e.message}`);
    }
  }

  export function init(
    conn: SSH.Connection,
    serverConfig: ServerConfig,
    serializer: TunnelConfig.Serializer
  ): Interface {
    return {
      portRange() {
        return serverConfig.portRange;
      },
      host() {
        return conn.configuration().host;
      },
      async connectedPorts() {
        const netstat = await conn.exec(
          `netstat -t -l -n | grep "tcp\\b" | awk '{print $4}'`
        );
        debug("netstat:", netstat);
        return netstat
          .split("\n")
          .filter(l => l !== "")
          .map(l => l.split(":")[1])
          .map(val => parseInt(val))
          .filter(val => !isNaN(val));
      },
      async tunnel(
        remotePort: number,
        localPort: number,
        listener: TunnelEventListener
      ) {
        const remoteAddr = "127.0.0.1";
        await conn.forwardIn(remoteAddr, remotePort);
        conn.on("tcp connection", (info, accept, reject) => {
          debug("TCP :: INCOMING CONNECTION:");
          debug(info);
          listener.connection && listener.connection(info);
          debug(`trying to connect to local server at port ${localPort}`);
          const localSocket = new Socket();
          localSocket.on("error", err => {
            reject();
            debug(err);
            if (err.message.includes("ECONNREFUSED") && listener.error) {
              listener.error(
                new Error(
                  `Failed to connect to local server. Details: ${err.message}. Is it up?`
                )
              );
            } else {
              listener.error && listener.error(err);
            }
          });
          localSocket.connect(localPort, () => {
            const channel = accept();
            channel.on("data", (data: any) => {
              debug("remote data", data.toString());
              listener.data && listener.data(data.toString());
            });
            channel.on("close", () => {
              debug("remote connection closed");
              listener.close && listener.close();
            });
            localSocket.pipe(channel).pipe(localSocket);
          });
        });
        debug(
          `forwarding connections from remote: ${remoteAddr}:${remotePort} to local port ${localPort}`
        );
      },
      async readTunnelConfig() {
        const rawConfig = await conn.exec(`cat ${serverConfig.tunnelConfig}`);
        const result = serializer.deserialize(rawConfig);
        return result;
      },
      async writeTunnelConfig(tunnels: TunnelConfig.Tunnels) {
        const str = serializer.serialize(tunnels);
        return await conn.exec(`echo "${str}" > ${serverConfig.tunnelConfig}`);
      },
      async restart() {
        // unfortunately traefik's hot reloading doesn't seem to work properly...
        return await conn.exec(serverConfig.restartCmd);
      }
    };
  }
}
