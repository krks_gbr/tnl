import * as Debug from "debug";
import { Remote } from "../remote";
import { TunnelConfig } from "../tunnel-config/index";

const debug = Debug("tnl:api");

function findAvailablePorts(
  portsTaken: number[],
  portRange: Remote.TunnelPortRange
) {
  const largestPort = portsTaken.sort()[portsTaken.length - 1];
  return [...Array(largestPort).keys()].filter(
    i =>
      !portsTaken.includes(i) &&
      i !== 0 &&
      i >= portRange[0] &&
      i <= portRange[1]
  );
}

export namespace API {
  const list = (remote: Remote.Interface) => async () => {
    return await remote.readTunnelConfig();
  };

  const add = (remote: Remote.Interface) => async (key: string) => {
    debug("adding new tunnel with key:", key);
    const tunnels = await list(remote)();

    const duplicate = !!tunnels.find(tunnel => tunnel.key === key);
    if (duplicate) {
      return Promise.reject(
        new Error(
          `There is already a tunnel named "${key}". You can't have multiple tunnels with the same key.`
        )
      );
    }
    const tunnelPortRange = remote.portRange();
    const host = remote.host();
    const portsTaken = tunnels.map(tunnel => tunnel.target.port).sort();
    const nextPort = (function getNextPort() {
      if (tunnels.length === 0) {
        return tunnelPortRange[0];
      }
      const availablePorts = findAvailablePorts(portsTaken, tunnelPortRange);
      if (availablePorts.length > 0) {
        return availablePorts[0];
      }
      const prevlargestPort = portsTaken[portsTaken.length - 1];
      if (prevlargestPort + 1 <= tunnelPortRange[1]) {
        return prevlargestPort + 1;
      }
      return null;
    })();
    if (!nextPort) {
      return Promise.reject(
        new Error(
          `Ran out of assignable ports in range ${tunnelPortRange.join(" - ")}`
        )
      );
    }
    const newTunnel: TunnelConfig.Tunnel = {
      key,
      virtualHost: `${key}.${host}`,
      target: {
        port: nextPort,
        url: host
      }
    };
    const newTunnels = tunnels.concat(newTunnel);
    await remote.writeTunnelConfig(newTunnels);
    await remote.restart();
    return newTunnels;
  };

  const remove = (remote: Remote.Interface) => async (key: string) => {
    const tunnels = await list(remote)();
    const newTunnels = tunnels.filter(tunnel => tunnel.key !== key);
    if (tunnels.length === newTunnels.length) {
      return Promise.reject(new Error(`No such tunnel: ${key}.`));
    }
    await remote.writeTunnelConfig(newTunnels);
    await remote.restart();
    return newTunnels;
  };

  export type Status = Array<{
    key: string;
    status: string;
  }>;
  const status = (remote: Remote.Interface) => async (): Promise<Status> => {
    const tunnels = await list(remote)();
    const connectedPorts = await remote.connectedPorts();
    return tunnels.map(t => {
      return {
        key: t.key,
        status: connectedPorts.includes(t.target.port)
          ? "connected"
          : "disconnected"
      };
    });
  };

  const makeTunnel = (remote: Remote.Interface) => async (
    key: string,
    localPort: number,
    listener?: Remote.TunnelEventListener
  ) => {
    const tunnels = await list(remote)();
    const tunnel = tunnels.find(tunnel => tunnel.key === key);
    if (!tunnel) {
      return Promise.reject(new Error(`No such tunnel: ${key}`));
    }
    return remote.tunnel(tunnel.target.port, localPort, listener);
  };

  export function init(remote: Remote.Interface) {
    return {
      list: list(remote),
      add: add(remote),
      remove: remove(remote),
      status: status(remote),
      tunnel: makeTunnel(remote)
    };
  }

  const remoteStub: Remote.Interface = {
    readTunnelConfig: () => Promise.resolve([]),
    writeTunnelConfig: () => Promise.resolve(""),
    connectedPorts: () => Promise.resolve([]),
    restart: () => Promise.resolve(""),
    host: () => "",
    portRange: () => [0, 0],
    tunnel: () => Promise.resolve()
  };
  const apiStub = init(remoteStub);
  export type Interface = typeof apiStub;
}







