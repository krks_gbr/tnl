import * as ora from "ora";
import * as program from "commander";
import * as Debug from "debug";
import * as table from "text-table";
import chalk from "chalk";
import { API } from "../api/index";
import { TunnelConfig } from "../tunnel-config/index";
import { Remote } from "../remote/index";

const debug = Debug("cli");

export function printTunnels(
  tunnels: TunnelConfig.Tunnels,
  status: API.Status
) {
  const formattedTunnels = tunnels.map(tunnel => {
    const st = status.find(s => s.key === tunnel.key).status;
    const statusColor = st === "connected" ? chalk.green : chalk.redBright;
    return [
      tunnel.key,
      tunnel.virtualHost,
      `${tunnel.target.url}:${tunnel.target.port}`,
      statusColor(st)
    ];
  });
  const data = [
    ["key", "virtualhost", "target", "status"].map(h => h.toUpperCase()),
    ...formattedTunnels
  ];
  const [header, ...rest] = table(data).split("\n");
  console.log("");
  console.log(chalk.underline(chalk.bold(` ${header}       `)));
  console.log(rest.map((l: string) => ` ${l}`).join("\n"));
  console.log("");
}

export namespace CLI {
  export function init(api: API.Interface) {
    program
      .command("list")
      .alias("ls")
      .description("list available tunnels")
      .action(async () => {
        const spinner = ora("Listing tunnels...").start();
        try {
          const tunnels = await api.list();
          if (tunnels.length > 0) {
            const status = await api.status();
            spinner.clear();
            printTunnels(tunnels, status);
          } else {
            spinner.fail(
              `No tunnels added yet. Use ${chalk.blue(
                "tnl add <key>"
              )} to add one.`
            );
          }
          process.exit(0);
        } catch (e) {
          spinner.fail(chalk.red(e.message));
          process.exit(1);
        }
      });

    program
      .command("add <key>")
      .description("add a new tunnel named <key>")
      .action(async key => {
        debug("adding ", key);
        const spinner = ora(`Adding new tunnel "${key}"...`).start();
        try {
          await api.add(key);
          spinner.succeed(chalk.green(`Added new tunnel "${key}".`));
          process.exit(0);
        } catch (e) {
          spinner.fail(chalk.red(e.message));
          process.exit(1);
        }
      });
    program
      .command("remove <key>")
      .alias("rm")
      .description("remove tunnel <key>")
      .action(async key => {
        const spinner = ora(`Removing tunnel "${key}"...`).start();
        try {
          await api.remove(key);
          spinner.succeed(chalk.green(`Removed tunnel "${key}".`));
          process.exit(0);
        } catch (e) {
          spinner.fail(chalk.red(e.message));
          process.exit(1);
        }
      });

    program
      .command("tnl <key> <localport>")
      .alias("connect")
      .description("tunnel traffic from tunnel <key> to local port <localport>")
      .action(async (key, port) => {
        const spinner = ora(
          `Connecting tunnel "${key}" to local port ${port}...`
        ).start();
        try {
          const listen: Remote.TunnelEventListener = {
            data(data: string) {
              console.log(chalk.blue(data));
            },
            error(err) {
              console.log(chalk.red(err.message));
            },
            close() {
              console.log(chalk.blue("Tunnel closed."));
            }
          };
          await api.tunnel(key, port, listen);
          spinner.succeed(
            chalk.green(`Connected tunnel "${key}" to local port ${port}...`)
          );
          console.log("");
        } catch (e) {
          spinner.fail(chalk.red(e.message));
          process.exit(1);
        }
      });
    return program;
  }
}
