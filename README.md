# tnl

## What's this?
A little command line tool to help make locally running servers accessible through a public URL. To use it, you need a server running  [Traefik](https://github.com/containous/traefik), a reverse proxy. What this tool basically does is it provides an interface to remotely manage a Traefik configuration file. This configuration file will contain virtual host rules to forward traffic to specific ports on your server, to which you can then establish reverse SSH tunnels from your local machine. **tnl** will help your with that too.

**Why Traefik?**


Because it's the most convenient reverse proxy to use with Docker. Though tnl could be easily extended to work with Nginx or Apache as well.


## Setup

### Prerequisites
1. A publicly accessible server running Traefik. You also need to make sure you can access this server through SSH using your public key.
2. You need node.js installed on your local machine.

### Installation
1. clone this repository
2. Install dependencies: `npm install` or `yarn install`
3. Build: `npm run build` or `yarn build`
4. Install: `npm install -g`
5. **tnl** should now be available at `/usr/local/bin/tnl`. Verify if the installation succeeded: `tnl`

### Configuration
#### On your local machine
Create a configuration file named `.tnlrc` in your home directory. It should be a json file containing the following values:

``` 
host: <host-name-of-your-server> 
username: <your-username-on-the-server>
privateKey (optional): <absolute-path-to-your-privatekey> defaults to $HOME/.ssh/id_rsa
```

complete example: 
``` json
{
  "host": "rndr.studio",
  "username": "dev",
  "privateKey": "/Users/krks/.ssh/id_rsa"
}
```

#### On the server
##### 1. Setup Traefik
If you don't have a running Traefik instance yet, follow [https://docs.traefik.io/](https://docs.traefik.io/) to set one up.
Once you've done that:
1. Create an empty file named `tnl.toml`. This is the file that **tnl** will manage.
2. Add a reference to this file to `traefik.toml`:
``` toml
[file]
watch = true
filename="path/to/tnl.toml"
```

##### 2. Create a json file named `.tnlrc.server` with the following values in your home directory on the server:
`portRange`: when creating a new virtual host, **tnl** will make traefik forward connections to a specific port on your server, to which it can then connect via a reverse ssh tunnel. the `portRange` value limits which ports it can use for this. Choose an obscure value, so that it doesn't conflict with other services that might be running on your server.

`tunnelConfig`: absolute path to your `tnl.toml`

`restartCmd`: **tnl** will need to restart Traefik whenever it updates the configuration file. Traefik should support hot reloading this file, but it doesn't seem to work at the moment. It will use this command to restart Traefik.

complete example:
``` json
{
  "portRange": [20000, 20050],
  "tunnelConfig": "/home/gbr/traefik/tnl.toml",
  "restartCmd": "docker restart traefik"
}
```

#### 3. Make sure `GatewayPorts` is set to `yes` in `/etc/ssh/sshd_config`


## Usage
Assuming everything is configured correctly and Traefik is running on your server, you should now be able to use **tnl**.

Simply run `tnl` to output usage information.


## Technical Details
**tnl** is written in TypeScript.
It uses [SSH2](https://github.com/mscdex/ssh2) for ssh stuff, [commander](https://github.com/tj/commander.js/) for the cli, [tomlify](https://github.com/jakwings/tomlify-j0.4) and [toml-j0.4](https://github.com/jakwings/toml-j0.4) for `toml` stuff, [chalk](https://github.com/chalk/chalk),  [text-table](https://www.npmjs.com/package/text-table) and [ora](https://www.npmjs.com/package/text-table) for rendering stuff.


## Contact
Gabor Kerekes

``` json
gabor@rndr.studio
gbr@krks.info
```
